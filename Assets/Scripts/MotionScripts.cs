﻿using UnityEngine;
using System.Collections;

public class MotionScripts {

	public static float linearTween(float t,float b,float c,float d)
	{
		return c*t/d + b;
	}

	public static Vector3 linearTween(float t,Vector3 b,Vector3 c,float d)
	{
		return c*t/d + b;
	}

	public static float easeInQuad(float t, float b, float c, float d) 
	{
		return c*(t/=d)*t + b;
	}

	public static Vector3 easeInQuad(float t, Vector3 b, Vector3 c, float d) 
	{
		return c*(t/=d)*t + b;
	}

	public static float easeOutQuad(float t, float b, float c, float d)  
	{
		return -c * (t/=d)*(t-2) + b;
	}

	public static Vector3 easeOutQuad(float t, Vector3 b, Vector3 c, float d)  
	{
		return -c * (t/=d)*(t-2) + b;
	}

	public static float easeInOutQuad(float t, float b, float c, float d)  
	{
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	}

	public static Vector3 easeInOutQuad(float t, Vector3 b, Vector3 c, float d)  
	{
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	}

	public static Rect easeInOutQuad(float t, Rect b, Rect e, float d)  
	{
		Rect c = new Rect();
		c.x = e.x - b.x;
		c.y = e.y - b.y;
		c.width = e.width - b.width;
		c.height = e.height - b.height;

		Rect ret = new Rect();
		ret.x = easeInOutQuad (t, b.x, c.x, d);
		ret.y = easeInOutQuad (t, b.y, c.y, d);
		ret.width = easeInOutQuad (t, b.width, c.width, d);
		ret.height = easeInOutQuad (t, b.height, c.height, d);
		return ret;
	}

	public static Vector2 easeInOutQuad(float t, Vector2 b, Vector2 c, float d)  
	{
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	}

	public static float easeInCubic(float t, float b, float c, float d)  
	{
		return c * Mathf.Pow (t/d, 3) + b;
	}
	
	public static Vector3 easeInCubic(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * Mathf.Pow (t/d, 3) + b;
	}

	public static float easeOutCubic(float t, float b, float c, float d)  
	{
		return c * (Mathf.Pow (t/d-1, 3) + 1) + b;
	}
	
	public static Vector3 easeOutCubic(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * (Mathf.Pow (t/d-1, 3) + 1) + b;
	}

	public static float easeInOutCubic(float t, float b, float c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * Mathf.Pow (t, 3) + b;
		return c/2 * (Mathf.Pow (t-2, 3) + 2) + b;
	}
	
	public static Vector3 easeInOutCubic(float t, Vector3 b, Vector3 c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * Mathf.Pow (t, 3) + b;
		return c/2 * (Mathf.Pow (t-2, 3) + 2) + b;
	}

	public static float easeInQuart(float t, float b, float c, float d)  
	{
		return c * Mathf.Pow (t/d, 4) + b;
	}
	
	public static Vector3 easeInQuart(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * Mathf.Pow (t/d, 4) + b;
	}

	public static float easeOutQuart(float t, float b, float c, float d)  
	{
		return -c * (Mathf.Pow (t/d-1, 4) - 1) + b;
	}
	
	public static Vector3 easeOutQuart(float t, Vector3 b, Vector3 c, float d)  
	{
		return -c * (Mathf.Pow (t/d-1, 4) - 1) + b;
	}

	public static float easeInOutQuart(float t, float b, float c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * Mathf.Pow (t, 4) + b;
		return -c/2 * (Mathf.Pow (t-2, 4) - 2) + b;
	}
	
	public static Vector3 easeInOutQuart(float t, Vector3 b, Vector3 c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * Mathf.Pow (t, 4) + b;
		return -c/2 * (Mathf.Pow (t-2, 4) - 2) + b;
	}

	public static float easeInQuint(float t, float b, float c, float d)  
	{
		return c * Mathf.Pow (t/d, 5) + b;
	}
	
	public static Vector3 easeInQuint(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * Mathf.Pow (t/d, 5) + b;
	}

	public static float easeOutQuint(float t, float b, float c, float d)  
	{
		return c * (Mathf.Pow (t/d-1, 5) + 1) + b;
	}
	
	public static Vector3 easeOutQuint(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * (Mathf.Pow (t/d-1, 5) + 1) + b;
	}

	public static float easeInOutQuint(float t, float b, float c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * Mathf.Pow (t, 5) + b;
		return c/2 * (Mathf.Pow (t-2, 5) + 2) + b;
	}
	
	public static Vector3 easeInOutQuint(float t, Vector3 b, Vector3 c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * Mathf.Pow (t, 5) + b;
		return c/2 * (Mathf.Pow (t-2, 5) + 2) + b;
	}

	public static float easeInSine(float t, float b, float c, float d)  
	{
		return c * (1 - Mathf.Cos(t/d * (Mathf.PI/2))) + b;
	}
	
	public static Vector3 easeInSine(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * (1 - Mathf.Cos(t/d * (Mathf.PI/2))) + b;
	}

	public static float easeOutSine(float t, float b, float c, float d)  
	{
		return c * Mathf.Sin(t/d * (Mathf.PI/2)) + b;
	}
	
	public static Vector3 easeOutSine(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * Mathf.Sin(t/d * (Mathf.PI/2)) + b;
	}

	public static float easeInOutSine(float t, float b, float c, float d)  
	{
		return c/2 * (1 - Mathf.Cos(Mathf.PI*t/d)) + b;
	}
	
	public static Vector3 easeInOutSine(float t, Vector3 b, Vector3 c, float d)  
	{
		return c/2 * (1 - Mathf.Cos(Mathf.PI*t/d)) + b;
	}

	public static float easeInExpo(float t, float b, float c, float d)  
	{
		return c * Mathf.Pow(2, 10 * (t/d - 1)) + b;
	}
	
	public static Vector3 easeInExpo(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * Mathf.Pow(2, 10 * (t/d - 1)) + b;
	}

	public static float easeOutExpo(float t, float b, float c, float d)  
	{
		return c * (-Mathf.Pow(2, -10 * t/d) + 1) + b;
	}
	
	public static Vector3 easeOutExpo(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * (-Mathf.Pow(2, -10 * t/d) + 1) + b;
	}

	public static float easeInOutExpo(float t, float b, float c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * Mathf.Pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Mathf.Pow(2, -10 * --t) + 2) + b;
	}
	
	public static Vector3 easeInOutExpo(float t, Vector3 b, Vector3 c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * Mathf.Pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Mathf.Pow(2, -10 * --t) + 2) + b;
	}

	public static float easeInCirc(float t, float b, float c, float d)  
	{
		return c * (1 - Mathf.Sqrt(1 - (t/=d)*t)) + b;
	}
	
	public static Vector3 easeInCirc(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * (1 - Mathf.Sqrt(1 - (t/=d)*t)) + b;
	}

	public static float easeOutCirc(float t, float b, float c, float d)  
	{
		return c * Mathf.Sqrt(1 - (t=t/d-1)*t) + b;
	}
	
	public static Vector3 easeOutCirc(float t, Vector3 b, Vector3 c, float d)  
	{
		return c * Mathf.Sqrt(1 - (t=t/d-1)*t) + b;
	}

	public static float easeInOutCirc(float t, float b, float c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * (1 - Mathf.Sqrt(1 - t*t)) + b;
		return c/2 * (Mathf.Sqrt(1 - (t-=2)*t) + 1) + b;
	}
	
	public static Vector3 easeInOutCirc(float t, Vector3 b, Vector3 c, float d)  
	{
		if ((t/=d/2) < 1)
			return c/2 * (1 - Mathf.Sqrt(1 - t*t)) + b;
		return c/2 * (Mathf.Sqrt(1 - (t-=2)*t) + 1) + b;
	}

	public static float easeInElastic(float t, float b, float c, float d)   
	{
		if (t==0) return b;
		if ((t/=d)==1) return b+c;
		float p = d*0.3f;
		float s;
		float a = 0;
		if (a==0 || a < Mathf.Abs(c)) {
			a = c;
			s = p/4;
		} else {
			s = p/(2*Mathf.PI) * Mathf.Asin (c/a);
		}
		return -(a*Mathf.Pow(2,10*(t-=1)) * Mathf.Sin( (t*d-s)*(2*Mathf.PI)/p )) + b;
	}

	public static Vector3 easeInElastic(float t, Vector3 b, Vector3 c, float d)   
	{
		Vector3 temp;
		temp.x = easeInElastic (t, b.x, c.x, d);
		temp.y = easeInElastic (t, b.y, c.y, d);
		temp.z = easeInElastic (t, b.z, c.z, d);
		return temp;
	}

	public static float easeOutElastic(float t, float b, float c, float d)   
	{
		if (t==0) return b;
		if ((t/=d)==1) return b+c;
		float p = d*0.3f;
		float s;
		float a = 0;
		if (a==0 || a < Mathf.Abs(c)) {
			a = c;
			s = p/4;
		} else {
			s = p/(2*Mathf.PI) * Mathf.Asin (c/a);
		}
		return (a*Mathf.Pow(2,-10*t) * Mathf.Sin( (t*d-s)*(2*Mathf.PI)/p ) + c + b);
	}

	public static Vector3 easeOutElastic(float t, Vector3 b, Vector3 c, float d)   
	{
		Vector3 temp;
		temp.x = easeOutElastic (t, b.x, c.x, d);
		temp.y = easeOutElastic (t, b.y, c.y, d);
		temp.z = easeOutElastic (t, b.z, c.z, d);
		return temp;
	}

	public static float easeInOutElastic(float t, float b, float c, float d)  
	{
		if (t==0) return b;
		if ((t/=d/2)==2) return b+c;
		float p = d*(0.3f*1.5f);
		float s;
		float a = 0;
		if (a==0 || a < Mathf.Abs(c)) {
			a = c;
			s = p/4;
		} else {
			s = p/(2*Mathf.PI) * Mathf.Asin (c/a);
		}
		if (t < 1f) return -0.5f*(a*Mathf.Pow(2f,10f*(t-=1f)) * Mathf.Sin( (t*d-s)*(2f*Mathf.PI)/p )) + b;
		return a*Mathf.Pow(2f,-10f*(t-=1f)) * Mathf.Sin( (t*d-s)*(2f*Mathf.PI)/p )*.5f + c + b;
	}

	public static Vector3 easeInOutElastic(float t, Vector3 b, Vector3 c, float d)   
	{
		Vector3 temp;
		temp.x = easeInOutElastic (t, b.x, c.x, d);
		temp.y = easeInOutElastic (t, b.y, c.y, d);
		temp.z = easeInOutElastic (t, b.z, c.z, d);
		return temp;
	}
	
	public static float easeInBack(float t, float b, float c, float d)  
	{
		float s = 1.70158f;
		return c*(t/=d)*t*((s+1f)*t - s) + b;
	}

	public static Vector3 easeInBack(float t, Vector3 b, Vector3 c, float d) 
	{
		Vector3 temp;
		temp.x = easeInBack (t, b.x, c.x, d);
		temp.y = easeInBack (t, b.y, c.y, d);
		temp.z = easeInBack (t, b.z, c.z, d);
		return temp;
	}

	public static float easeOutBack(float t, float b, float c, float d)
	{
		float s = 1.70158f;
		return c*((t=t/d-1f)*t*((s+1f)*t + s) + 1f) + b;
	}

	public static Vector3 easeOutBack(float t, Vector3 b, Vector3 c, float d) 
	{
		Vector3 temp;
		temp.x = easeOutBack (t, b.x, c.x, d);
		temp.y = easeOutBack (t, b.y, c.y, d);
		temp.z = easeOutBack (t, b.z, c.z, d);
		return temp;
	}

	public static float easeInOutBack(float t, float b, float c, float d) 
	{
		float s = 1.70158f;
		if ((t/=d/2f) < 1f) return c/2f*(t*t*(((s*=(1.525f))+1f)*t - s)) + b;
		return c/2f*((t-=2f)*t*(((s*=(1.525f))+1f)*t + s) + 2f) + b;
	}

	public static Vector3 easeInOutBack(float t, Vector3 b, Vector3 c, float d) 
	{
		Vector3 temp;
		temp.x = easeInOutBack (t, b.x, c.x, d);
		temp.y = easeInOutBack (t, b.y, c.y, d);
		temp.z = easeInOutBack (t, b.z, c.z, d);
		return temp;
	}

	public static float easeOutInBack(float t, float b, float c, float d) 
	{
		if (t < d/2f) return MotionScripts.easeOutBack (t*2f, b, c/2f, d);
		return MotionScripts.easeInBack((t*2f)-d, b+c/2f, c/2f, d);
	}

	public static Vector3 easeOutInBack(float t, Vector3 b, Vector3 c, float d) 
	{
		Vector3 temp;
		temp.x = easeOutInBack (t, b.x, c.x, d);
		temp.y = easeOutInBack (t, b.y, c.y, d);
		temp.z = easeOutInBack (t, b.z, c.z, d);
		return temp;
	}

	public static float easeInBounce(float t, float b, float c, float d) 
	{
		return c - MotionScripts.easeOutBounce (d-t, 0, c, d) + b;
	}

	public static Vector3 easeInBounce(float t, Vector3 b, Vector3 c, float d) 
	{
		Vector3 temp;
		temp.x = easeInBounce (t, b.x, c.x, d);
		temp.y = easeInBounce (t, b.y, c.y, d);
		temp.z = easeInBounce (t, b.z, c.z, d);
		return temp;
	}

	public static float easeOutBounce(float t, float b, float c, float d) 
	{
		if ((t/=d) < (1f/2.75f)) {
			return c*(7.5625f*t*t) + b;
		} else if (t < (2f/2.75f)) {
			return c*(7.5625f*(t-=(1.5f/2.75f))*t + 0.75f) + b;
		} else if (t < (2.5f/2.75f)) {
			return c*(7.5625f*(t-=(2.25f/2.75f))*t + 0.9375f) + b;
		} else {
			return c*(7.5625f*(t-=(2.625f/2.75f))*t + 0.984375f) + b;
		}
	}

	public static Vector3 easeOutBounce(float t, Vector3 b, Vector3 c, float d) 
	{
		Vector3 temp;
		temp.x = easeOutBounce (t, b.x, c.x, d);
		temp.y = easeOutBounce (t, b.y, c.y, d);
		temp.z = easeOutBounce (t, b.z, c.z, d);
		return temp;
	}

	public static Vector2 easeOutBounce(float t, Vector2 b, Vector2 c, float d) 
	{
		Vector2 temp;
		temp.x = easeOutBounce (t, b.x, c.x, d);
		temp.y = easeOutBounce (t, b.y, c.y, d);
		return temp;
	}

	public static float easeInOutBounce(float t, float b, float c, float d) 
	{
	 	if (t < d/2f) return MotionScripts.easeInBounce (t*2f, 0f, c, d) * 0.5f + b;
		else return MotionScripts.easeOutBounce (t*2f-d, 0f, c, d) * 0.5f + c*0.5f + b;
	}

	public static Vector3 easeInOutBounce(float t, Vector3 b, Vector3 c, float d) 
	{
		Vector3 temp;
		temp.x = easeInOutBounce (t, b.x, c.x, d);
		temp.y = easeInOutBounce (t, b.y, c.y, d);
		temp.z = easeInOutBounce (t, b.z, c.z, d);
		return temp;
	}

	public static Vector2 easeInOutBounce(float t, Vector2 b, Vector2 c, float d) 
	{
		Vector2 temp;
		temp.x = easeInOutBounce (t, b.x, c.x, d);
		temp.y = easeInOutBounce (t, b.y, c.y, d);
		return temp;
	}

	public static float easeOutInBounce(float t, float b, float c, float d) 
	{
		if (t < d/2f) return MotionScripts.easeOutBounce (t*2f, b, c/2f, d);
		return MotionScripts.easeInBounce((t*2f)-d, b+c/2f, c/2f, d);
	}

	public static Vector3 easeOutInBounce(float t, Vector3 b, Vector3 c, float d) 
	{
		Vector3 temp;
		temp.x = easeOutInBounce (t, b.x, c.x, d);
		temp.y = easeOutInBounce (t, b.y, c.y, d);
		temp.z = easeOutInBounce (t, b.z, c.z, d);
		return temp;
	}
}
