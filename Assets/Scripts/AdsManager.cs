﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;
using System.Diagnostics;

public class AdsManager : MonoBehaviour {
	
	private static AdsManager _instance = null;

	enum AdType {AdBanner, InterstitialAd};

	private BannerView bannerView;
	private InterstitialAd interstitial;

	private string adBannerUnitId = "unexpected_platform";
	private string adInterstitialUnitId = "unexpected_platform";

	int countGamePlayed = 0;

	private bool isAdBannerLoadedSuccessfully = false;
	private bool isInterstitialRequestedSuccessfully = false;

	public bool IsAdBannerLoadedSuccessfully
	{
		get
		{
			return this.isAdBannerLoadedSuccessfully;
		}
	}

	public bool IsInterstitialRequestedSuccessfully
	{
		get
		{
			return this.isInterstitialRequestedSuccessfully;
		}
	}

	public static AdsManager Instance 
	{
		get 
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<AdsManager>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}

	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}

	public int CountGamePlayed
	{
		get 
		{
			return countGamePlayed;
		} 
		set
		{
			countGamePlayed = value;
		}
	}

	[Conditional("UNITY_EDITOR")]
	void GetAdUnitIdUnityEditor(AdType adType)
	{
		if (adType == AdType.AdBanner) adBannerUnitId = "unused";
		else if (adType == AdType.InterstitialAd) adInterstitialUnitId = "unused";
	}
	
	[Conditional("UNITY_ANDROID")]
	void GetAdUnitIdAndroid(AdType adType)
	{
		if (adType == AdType.AdBanner) adBannerUnitId = "ca-app-pub-8327128320822778/7957862848";
		else if (adType == AdType.InterstitialAd) adInterstitialUnitId = "ca-app-pub-8327128320822778/6341528844";
	}

	[Conditional("UNITY_IPHONE")]
	void GetAdUnitIdIphone(AdType adType)
	{
		if (adType == AdType.AdBanner) adBannerUnitId = "ca-app-pub-8327128320822778/7539060446";
		else if (adType == AdType.InterstitialAd) adInterstitialUnitId = "ca-app-pub-8327128320822778/1492526842";
	}

	public void RequestBanner()
	{
		GetAdUnitIdUnityEditor (AdType.AdBanner);
		GetAdUnitIdAndroid (AdType.AdBanner);
		GetAdUnitIdIphone (AdType.AdBanner);

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adBannerUnitId, AdSize.SmartBanner, AdPosition.Bottom);
		// Register for ad events.
		bannerView.AdLoaded += HandleAdLoaded;
		bannerView.AdFailedToLoad += HandleAdFailedToLoad;
		bannerView.AdOpened += HandleAdOpened;
		bannerView.AdClosing += HandleAdClosing;
		bannerView.AdClosed += HandleAdClosed;
		bannerView.AdLeftApplication += HandleAdLeftApplication;
		// Load a banner ad.
		bannerView.LoadAd(createAdRequest());
	}
	
	public void RequestInterstitial()
	{
		GetAdUnitIdUnityEditor (AdType.InterstitialAd);
		GetAdUnitIdAndroid (AdType.InterstitialAd);
		GetAdUnitIdIphone (AdType.InterstitialAd);
		
		// Create an interstitial.
		interstitial = new InterstitialAd(adInterstitialUnitId);
		// Register for ad events.
		interstitial.AdLoaded += HandleInterstitialLoaded;
		interstitial.AdFailedToLoad += HandleInterstitialFailedToLoad;
		interstitial.AdOpened += HandleInterstitialOpened;
		interstitial.AdClosing += HandleInterstitialClosing;
		interstitial.AdClosed += HandleInterstitialClosed;
		interstitial.AdLeftApplication += HandleInterstitialLeftApplication;

		// Load an interstitial ad.
		interstitial.LoadAd(createAdRequest());
	}
	
	// Returns an ad request with custom ad targeting.
	public AdRequest createAdRequest()
	{
		return new AdRequest.Builder()
			.AddTestDevice(AdRequest.TestDeviceSimulator)
				.AddTestDevice("9F55EFE03491B649F19CB2C50C8D2198")
				.AddTestDevice("40442a80bd6387ae8c9f676c9db23deb")
				.AddKeyword("game")
				.SetGender(Gender.Male)
				.SetBirthday(new DateTime(1985, 1, 1))
				.TagForChildDirectedTreatment(false)
				.AddExtra("color_bg", "9B30FF")
				.Build();
		
	}
	
	public void ShowInterstitial()
	{
		if (interstitial.IsLoaded())
		{
			print("Interstitial is shown");
			interstitial.Show();
		}
		else
		{
			print("Interstitial is not ready yet.");
		}
	}
	
	#region Banner callback handlers
	
	public void HandleAdLoaded(object sender, EventArgs args)
	{
		print("HandleAdLoaded event received.");
		isAdBannerLoadedSuccessfully = true;
	}
	
	public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("HandleFailedToReceiveAd event received with message: " + args.Message);

		isAdBannerLoadedSuccessfully = false;
	}
	
	public void HandleAdOpened(object sender, EventArgs args)
	{
		print("HandleAdOpened event received");
	}
	
	void HandleAdClosing(object sender, EventArgs args)
	{
		print("HandleAdClosing event received");
	}
	
	public void HandleAdClosed(object sender, EventArgs args)
	{
		print("HandleAdClosed event received");
	}
	
	public void HandleAdLeftApplication(object sender, EventArgs args)
	{
		print("HandleAdLeftApplication event received");
	}
	
	#endregion
	
	#region Interstitial callback handlers
	
	public void HandleInterstitialLoaded(object sender, EventArgs args)
	{
		isInterstitialRequestedSuccessfully = true;
	}
	
	public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
		isInterstitialRequestedSuccessfully = false;
	}
	
	public void HandleInterstitialOpened(object sender, EventArgs args)
	{
		print("HandleInterstitialOpened event received");
	}
	
	void HandleInterstitialClosing(object sender, EventArgs args)
	{
		print("HandleInterstitialClosing event received");
	}
	
	public void HandleInterstitialClosed(object sender, EventArgs args)
	{
		print("HandleInterstitialClosed event received");
		RequestInterstitial ();

	}
	
	public void HandleInterstitialLeftApplication(object sender, EventArgs args)
	{
		print("HandleInterstitialLeftApplication event received");
	}
	
	#endregion
}
