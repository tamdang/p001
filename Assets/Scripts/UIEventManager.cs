﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Diagnostics;
using UnityEngine.SocialPlatforms.GameCenter;

#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class UIEventManager : MonoBehaviour {

	public float TransitionTime;
	public GameObject rateTheGamePanel;
	private static UIEventManager _instance = null;
	public GameObject OptionPanel;
	public GameObject BackGround;
	Vector2 OptionAnchorPositionShow;
	Vector2 OptionAnchorPositionHide;
	bool isAtMM = true;

	// Use this for initialization
	void Start () {
		Image panelImage = OptionPanel.GetComponent<Image> ();
		OptionAnchorPositionShow = new Vector2(panelImage.rectTransform.anchoredPosition.x,panelImage.rectTransform.anchoredPosition.y);
		OptionAnchorPositionHide = new Vector2 (panelImage.rectTransform.anchoredPosition.x, panelImage.rectTransform.anchoredPosition.y - panelImage.rectTransform.rect.height);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static UIEventManager Instance 
	{
		get 
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<UIEventManager>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}
	
	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}

	[Conditional("UNITY_ANDROID")]
	void RateTheGameAndroid()
	{
		Application.OpenURL("market://details?id=com.tamdang.biggernumber");
	}

	[Conditional("UNITY_IPHONE")]
	void RateTheGameIOS()
	{
		Application.OpenURL("itms-apps://itunes.apple.com/app/com.tamdang.biggernumber");
	}

	public void RateTheGame()
	{
		RateTheGameAndroid ();
		RateTheGameIOS ();
	}

	public void ShowHideOption(bool alwayHide)
	{
		if(OptionPanel.activeSelf)
		{
			StartCoroutine ("OptionDownAnimation", OptionPanel);
		}
		else
		{
			if(alwayHide) return;
			StartCoroutine ("OptionUpAnimation", OptionPanel);
		}
	}
	
	public void OnFlyIn(GameObject panel)
	{
		StartCoroutine ("FlyIn", panel);
	}

	public void OnFlyOut(GameObject panel)
	{
		StartCoroutine ("FlyOut", panel);
	}

	public void OnLeaderboardClick()
	{
		SocialManager.ShowLeaderboard ();
	}

	public void OnAchievementClick()
	{
		SocialManager.ShowAchievement ();
	}

	IEnumerator OptionUpAnimation(GameObject panel) {
		Image panelImage = panel.GetComponent<Image> ();
		float t = 0;
		panelImage.gameObject.SetActive (true);
		
		while(t<TransitionTime)
		{
			t+=Time.deltaTime*1000;
			panelImage.rectTransform.anchoredPosition = MotionScripts.easeInOutQuad(t,OptionAnchorPositionHide,OptionAnchorPositionShow-OptionAnchorPositionHide,TransitionTime);
			yield return null;
		}
		
		panelImage.rectTransform.anchoredPosition = OptionAnchorPositionShow;
	}

	IEnumerator OptionDownAnimation(GameObject panel) {
		Image panelImage = panel.GetComponent<Image> ();
		float t = 0;
		while(t<TransitionTime)
		{
			t+=Time.deltaTime*1000;
			panelImage.rectTransform.anchoredPosition = MotionScripts.easeInOutQuad(t,OptionAnchorPositionShow,OptionAnchorPositionHide-OptionAnchorPositionShow,TransitionTime);
			yield return null;
		}
		
		panelImage.rectTransform.anchoredPosition = OptionAnchorPositionHide;
		panelImage.gameObject.SetActive (false);
	}


	IEnumerator FlyIn(GameObject panel) {
		Image panelImage = panel.GetComponent<Image> ();
		float t = 0;
		Vector2 anchoredPosition0 = new Vector2(-Screen.width,panelImage.rectTransform.anchoredPosition.y);
		Vector2 anchoredPosition1 = new Vector2 (0, panelImage.rectTransform.anchoredPosition.y);
		panelImage.gameObject.SetActive (true);

		while(t<TransitionTime)
		{
			t+=Time.deltaTime*1000;
			panelImage.rectTransform.anchoredPosition = MotionScripts.easeInOutQuad(t,anchoredPosition0,anchoredPosition1-anchoredPosition0,TransitionTime);
			yield return null;
		}

		panelImage.rectTransform.anchoredPosition = anchoredPosition1;
	}


	IEnumerator FlyOut(GameObject panel) {
		Image panelImage = panel.GetComponent<Image> ();
		float t = 0;
		Vector2 anchoredPosition0 = panelImage.rectTransform.anchoredPosition;
		Vector2 anchoredPosition1 = new Vector2 (Screen.width, panelImage.rectTransform.anchoredPosition.y);
		while(t<TransitionTime)
		{
			t+=Time.deltaTime*1000;
			panelImage.rectTransform.anchoredPosition = MotionScripts.easeInOutQuad(t,anchoredPosition0,anchoredPosition1-anchoredPosition0,TransitionTime);
			yield return null;
		}
		
		panelImage.rectTransform.anchoredPosition = anchoredPosition1;
		panelImage.gameObject.SetActive (false);
	}	

	IEnumerator FlyOutRateTheGame(GameObject panel) {
		Image panelImage = panel.GetComponent<Image> ();
		float t = 0;
		Vector2 anchoredPosition0 = panelImage.rectTransform.anchoredPosition;
		Vector2 anchoredPosition1 = new Vector2 (Screen.width, panelImage.rectTransform.anchoredPosition.y);
		while(t<TransitionTime)
		{
			t+=Time.deltaTime*1000;
			panelImage.rectTransform.anchoredPosition = MotionScripts.easeInOutQuad(t,anchoredPosition0,anchoredPosition1-anchoredPosition0,TransitionTime);
			yield return null;
		}
		
		panelImage.rectTransform.anchoredPosition = anchoredPosition1;
		panelImage.gameObject.SetActive (false);

		Camera.main.GetComponent<GameController> ().OnPlay ();
	}	

	public void OnSoundClick(Button button)
	{

		Image img = button.GetComponent<Image> ();
		string imgSpriteName = GameIds.ResourceName.ImageFOLDER + img.sprite.name;
		if(GameIds.ResourceName.SoundBGOn.Equals(imgSpriteName))
		{
			button.image.sprite = Resources.Load<Sprite>(GameIds.ResourceName.SoundBGOff);
			Camera.main.GetComponent<GameController>().MuteSound(true);
		}
		else if(GameIds.ResourceName.SoundBGOff.Equals(imgSpriteName))
		{
			button.image.sprite = Resources.Load<Sprite>(GameIds.ResourceName.SoundBGOn);
			Camera.main.GetComponent<GameController>().MuteSound(false);
		}
		Camera.main.GetComponent<GameController> ().SaveGameSetting ();
	}
	
	public void OnSFXClick(Button button)
	{
		Image img = button.GetComponent<Image> ();
		string imgSpriteName = GameIds.ResourceName.ImageFOLDER + img.sprite.name;
		if(GameIds.ResourceName.SoundEffectOn.Equals(imgSpriteName))
		{
			button.image.sprite = Resources.Load<Sprite>(GameIds.ResourceName.SoundEffectOff);
			Camera.main.GetComponent<GameController>().MuteSFX(true);
		}
		else if(GameIds.ResourceName.SoundEffectOff.Equals(imgSpriteName))
		{
			button.image.sprite = Resources.Load<Sprite>(GameIds.ResourceName.SoundEffectOn);
			Camera.main.GetComponent<GameController>().MuteSFX(false);
		}
		Camera.main.GetComponent<GameController> ().SaveGameSetting ();
	}
	
	public void OnCreditClick()
	{
		
	}
	
	public void CreditClose(GameObject creditPanel)
	{
		creditPanel.SetActive (false);

		if(isAtMM)
		{
			BackGround.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Images/BG_MM");
		}

	}

	public void CreditOpen(GameObject creditPanel)
	{
		isAtMM = BackGround.GetComponent<Image> ().sprite.name.Equals ("BG_MM");
//		UnityEngine.Debug.Log (BackGround.GetComponent<Image> ().sprite.name);
		if(isAtMM)
		{
			BackGround.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Images/BG");
		}
		creditPanel.SetActive (true);
	}
}
