﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

//	public GUIText guiText;

	bool isPaused  = false;
	public float startTime ; //(in seconds)
	float timeRemaining ; //(in seconds)
	float time0 = 0; //(in seconds)
	float percent = 50;
	public Image Fill;
	public float BonusEffectDuration;

	public string GameControllerName;

	public void SetTime(float StartTime)
	{
		startTime = StartTime;
		timeRemaining = startTime;
		percent = 50;
	}

	public void StartBonuseffect()
	{
		StartCoroutine("BonusEffect");
	}

	IEnumerator BonusEffect() {
		float t = 0;
		Fill.sprite = Resources.Load<Sprite>("Images/bar_fill_bonus");
		while(t<BonusEffectDuration)
		{
			t+=Time.deltaTime;
			yield return null;
		}
		Fill.sprite = Resources.Load<Sprite>("Images/bar_fill");
	}

	// Use this for initialization
	void Start () {
		PauseClock ();
	}

	public void UpdateTimeRemaining(float deltaTime)
	{
		time0 = time0 + deltaTime/1000;
		if (time0>Time.time + startTime)
		{
			time0 = Time.time + startTime;
		}
	}

	void DoCountdown ()
	{
		timeRemaining = startTime + time0 - Time.time;

		percent = timeRemaining/(startTime*2);
		Fill.rectTransform.localScale = new Vector3(percent,Fill.rectTransform.localScale.y,Fill.rectTransform.localScale.z);

		if (timeRemaining < 0)
		{
			timeRemaining = 0;
			isPaused = true;
			TimeIsUp();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!isPaused)
		{
			// make sure the timer is not paused
			DoCountdown();
		}
	}

	public void ResetTimer()
	{
		Fill.rectTransform.localScale = new Vector3(0.5f,Fill.rectTransform.localScale.y,Fill.rectTransform.localScale.z);
	}

	public void StartCountDown()
	{
		time0 = Time.time;
		UnpauseClock ();
	}

	public void StopCountDown()
	{
		PauseClock ();
	}

	void PauseClock()
	{
		isPaused = true;
	}

	void UnpauseClock()
	{
		isPaused = false;
	}

	void TimeIsUp()
	{
		if(GameObject.Find (GameControllerName))
		{
			GameObject.Find (GameControllerName).SendMessage ("TimeIsUpListener",startTime,SendMessageOptions.DontRequireReceiver);
		}
	}
}
