﻿using UnityEngine;
using System.Collections;

public class NumberGenerator {

	public static bool enableCheat = false;
	public static bool enableCheatOneTwo = false;
	static int lastGenerate = -1; //0: left bigger than right, 1: right bigger than left

	static int GetLevel(int score, int levelMixing)
	{
		int level = 1;

		if(score < 10)
		{
			level = 1;
		}
		else if(score < 20)
		{
			level = 2;
		}
		else if(score < 30)
		{
			level = 3;
		}
		else if(score < 40)
		{
			level = 4;
		}
		else if(score < 57)
		{
			level = 1;
		}
		else if(score < 67)
		{
			level = 5;
		}
		else if(score < 80)
		{
			level = 6;
		}
		else if(score < 90)
		{
			level = 2;
		}
		else if (score < 100)
		{
			level = 6;
		}
		else 
		{
			level = 7;
		}

		if( score >3 && Random.Range (1, 100) > levelMixing)
		{
			level ++;
			if(level > 9) level = 9;
		}
		return level;
	}

	
	static int RandomByNumberLong(int numberLong)
	{
		int min = Mathf.RoundToInt(Mathf.Pow (10.0f, (float)numberLong-1));
		int max = Mathf.RoundToInt(Mathf.Pow (10.0f, (float)numberLong));
		
		return Random.Range (min, max);
	}

	public static void GenerateNumbers(int score, int levelMixing, out int LeftNumber, out int RightNumber)
	{
		int longNumber=1;
		int maxLong = 3;
		int similarity = 0;

		int level = GetLevel(score, levelMixing);
		switch (level)
		{
		case 1:
			longNumber = 1;
			similarity = 0;
			break;
		case 2:
			longNumber = 2;
			similarity = 0;
			break;
		case 3:
			longNumber = 2;
			similarity = 1;
			break;
		case 4:
			longNumber = 3;
			similarity = 0;
			break;
		case 5:
			longNumber = 3;
			similarity = 1;
			break;
		case 6:
			longNumber = 3;
			similarity = 2;
			break;
		case 7:
			longNumber = Random.Range (1, maxLong+1);
			similarity = Random.Range(0,longNumber);
			break;
		default:
			longNumber = Random.Range (1, maxLong+1);
			similarity = Random.Range(0,longNumber);
			break;
		}

		int SamePart = RandomByNumberLong (similarity);
		int DifferentPartLeft = 0;
		int DifferentPartRight = 0;
		do
		{
			DifferentPartLeft = RandomByNumberLong(longNumber-similarity);
			DifferentPartRight = RandomByNumberLong(longNumber-similarity);
		}while(DifferentPartLeft == DifferentPartRight);

		if(enableCheat)
		{
			if(lastGenerate<0)
			{
				lastGenerate = DifferentPartLeft > DifferentPartRight?0:1;
			}
			else if (lastGenerate == 0)
			{
				int tempt = 0;
				if(DifferentPartLeft > DifferentPartRight)
				{
					tempt = DifferentPartRight;
					DifferentPartRight = DifferentPartLeft;
					DifferentPartLeft = tempt;
				}

				if(enableCheatOneTwo) //ESLE: ONE side cheat
				   lastGenerate = 1;
			}
			else //lastGenerate == 1 
			{
				int tempt = 0;
				if(DifferentPartRight > DifferentPartLeft)
				{
					tempt = DifferentPartRight;
					DifferentPartRight = DifferentPartLeft;
					DifferentPartLeft = tempt;
					lastGenerate = 0;
				}
				if(enableCheatOneTwo) //ESLE: ONE side cheat
					lastGenerate = 0;
			}
		}

		LeftNumber = SamePart*Mathf.RoundToInt(Mathf.Pow (10.0f, (float)(longNumber-similarity)))+DifferentPartLeft;
		RightNumber = SamePart*Mathf.RoundToInt(Mathf.Pow (10.0f, (float)(longNumber-similarity)))+DifferentPartRight;
	}

}
