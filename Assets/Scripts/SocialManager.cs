using UnityEngine;
using System.Collections;
#if UNITY_ANDROID
using GooglePlayGames;
#endif
using UnityEngine.SocialPlatforms;
using GoogleMobileAds.Api;
using System;
using System.Diagnostics;
using UnityEngine.SocialPlatforms.GameCenter;

public enum AchievementId {IAm20, IAm40, IAm60, ALifeTime, LeftHandPerson, RightHandPerson, LeftThankGiving, RightThankGiving, OneTwo};

public class SocialManager{

	static ILeaderboard m_Leaderboard;

	public static void Authenticate()
	{
		GGPAuthenticate ();
		GCPAuthenticate ();
	}

	public static void ReportScore(int score)
	{
		GPReportScore (score);
		GCReportScore (score);
	}

	public static void UnlockAchievement(AchievementId achievementId)
	{
		GPUnlockAchivement (achievementId);
		GCUnlockAchivement (achievementId);
	}

	public static void ShowLeaderboard()
	{
		GPShowLeaderboard ();
		GCShowLeaderboard ();
	}

	public static void ShowAchievement()
	{
		GPShowAchievement ();
		GCShowAchievement ();
	}


	[Conditional("UNITY_ANDROID")]
	static void GGPAuthenticate()
	{
#if UNITY_ANDROID
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
#endif

		Social.localUser.Authenticate((bool success) => {
			// handle success or failure
		});
	}

	[Conditional("UNITY_IPHONE")]
	static void GCPAuthenticate()
	{
		// THIS CALL NEEDS OT BE MADE BEFORE WE CAN PROCEED TO OTHER CALLS IN THE Social API
		Social.localUser.Authenticate ((bool success) => {
			// handle success or failure
		});
	}
	
	[Conditional("UNITY_ANDROID")]
	static void GPShowLeaderboard()
	{
		if(!Social.Active.localUser.authenticated)
		{
			Social.localUser.Authenticate((bool success) => {
				// handle success or failure
			});
			
		}
		if (Social.Active.localUser.authenticated)
		{
#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(GameIds.GPLeaderboardId);
#endif
		}
	}

	[Conditional("UNITY_IPHONE")]
	static void GCShowLeaderboard()
	{
		if(!Social.Active.localUser.authenticated)
		{
			Social.localUser.Authenticate((bool success) => {
				// handle success or failure
			});
			
		}
		if (Social.Active.localUser.authenticated)
		{
			Social.Active.ShowLeaderboardUI();
		}
	}

	[Conditional("UNITY_ANDROID")]
	static void GPReportScore(int Score)
	{
		if(Social.Active.localUser.authenticated)
		{
			Social.ReportScore(Score, GameIds.GPLeaderboardId, (bool success) => {
				// handle success or failure
			});
		}
	}

	[Conditional("UNITY_IPHONE")]
	static void GCReportScore(int Score)
	{
		if(Social.Active.localUser.authenticated)
		{
			Social.ReportScore(Score, GameIds.GCleaderboardId, (bool success) => {
				// handle success or failure
			});
		}
	}

	[Conditional("UNITY_ANDROID")]
	static void GPShowAchievement()
	{
		if(!Social.Active.localUser.authenticated)
		{
			Social.localUser.Authenticate((bool success) => {
				// handle success or failure
			});
			
		}
		if (Social.Active.localUser.authenticated)
		{
			// show achievements UI
			Social.ShowAchievementsUI();
		}
	}

	[Conditional("UNITY_IPHONE")]
	static void GCShowAchievement()
	{
		if(!Social.Active.localUser.authenticated)
		{
			Social.localUser.Authenticate((bool success) => {
				// handle success or failure
			});
			
		}
		if (Social.Active.localUser.authenticated)
		{
			// show achievements UI
			Social.ShowAchievementsUI();
		}
	}

	[Conditional("UNITY_ANDROID")]
	static void GPUnlockAchivement(AchievementId achivementId)
	{
		switch (achivementId)
		{
		case AchievementId.IAm20:
			Social.ReportProgress(GameIds.GPAchievements.IAm20, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		case AchievementId.IAm40:
			Social.ReportProgress(GameIds.GPAchievements.IAm40, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		case AchievementId.IAm60:
			Social.ReportProgress(GameIds.GPAchievements.IAm60, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		case AchievementId.ALifeTime:
			Social.ReportProgress(GameIds.GPAchievements.ALifeTime, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		case AchievementId.LeftHandPerson:
			Social.ReportProgress(GameIds.GPAchievements.LeftHandPerson, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		case AchievementId.RightHandPerson:
			Social.ReportProgress(GameIds.GPAchievements.RightHandPerson, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		case AchievementId.LeftThankGiving:
			Social.ReportProgress(GameIds.GPAchievements.LeftThankGiving, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		case AchievementId.RightThankGiving:
			Social.ReportProgress(GameIds.GPAchievements.RightThankGiving, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		case AchievementId.OneTwo:
			Social.ReportProgress(GameIds.GPAchievements.OneTwo, 100.0f, (bool success) => {
				// handle success or failure
			});
			break;
		}

	
	}
	
	[Conditional("UNITY_IPHONE")]
	static void GCUnlockAchivement(AchievementId achivementId)
	{
		switch (achivementId)
		{
		case AchievementId.IAm20:
			Social.ReportProgress(GameIds.GCAchievements.IAm20, 100.0f,DisplayAchievementPopup);
			break;
		case AchievementId.IAm40:
			Social.ReportProgress(GameIds.GCAchievements.IAm40, 100.0f, DisplayAchievementPopup);
			break;
		case AchievementId.IAm60:
			Social.ReportProgress(GameIds.GCAchievements.IAm60, 100.0f, DisplayAchievementPopup);
			break;
		case AchievementId.ALifeTime:
			Social.ReportProgress(GameIds.GCAchievements.ALifeTime, 100.0f,DisplayAchievementPopup);
			break;
		case AchievementId.LeftHandPerson:
			Social.ReportProgress(GameIds.GCAchievements.LeftHandPerson, 100.0f, DisplayAchievementPopup);
			break;
		case AchievementId.RightHandPerson:
			Social.ReportProgress(GameIds.GCAchievements.RightHandPerson, 100.0f, DisplayAchievementPopup);
			break;
		case AchievementId.LeftThankGiving:
			Social.ReportProgress(GameIds.GCAchievements.LeftThankGiving, 100.0f, DisplayAchievementPopup);
			break;
		case AchievementId.RightThankGiving:
			Social.ReportProgress(GameIds.GCAchievements.RightThankGiving, 100.0f,DisplayAchievementPopup);
			break;
		case AchievementId.OneTwo:
			Social.ReportProgress(GameIds.GCAchievements.OneTwo, 100.0f, DisplayAchievementPopup);
			break;
		}
	}

//	[Conditional("UNITY_IPHONE")]
	static void DisplayAchievementPopup(bool success)
	{
		if(success)
		{
			UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
		}
	}


	#region Game Center get more detail of the leaderboard and the achievements

	///////////////////////////////////////////////////
	// INITAL AUTHENTICATION (MUST BE DONE FIRST)
	///////////////////////////////////////////////////
	
	// THIS FUNCTION GETS CALLED WHEN AUTHENTICATION COMPLETES
	// NOTE THAT IF THE OPERATION IS SUCCESSFUL Social.localUser WILL CONTAIN DATA FROM THE GAME CENTER SERVER
	void ProcessAuthentication (bool success) {
		if (success) {
			UnityEngine.Debug.Log ("Authenticated, checking achievements");
			
			// MAKE REQUEST TO GET LOADED ACHIEVEMENTS AND REGISTER A CALLBACK FOR PROCESSING THEM
			Social.LoadAchievements (ProcessLoadedAchievements); // ProcessLoadedAchievements FUNCTION CAN BE FOUND BELOW
			
			Social.LoadScores(GameIds.GCleaderboardId, scores => {
				if (scores.Length > 0) {
					// SHOW THE SCORES RECEIVED
					UnityEngine.Debug.Log ("Received " + scores.Length + " scores");
					string myScores = "Leaderboard: \n";
					foreach (IScore score in scores)
						myScores += "\t" + score.userID + " " + score.formattedValue + " " + score.date + "\n";
					UnityEngine.Debug.Log (myScores);
				}
				else
					UnityEngine.Debug.Log ("No scores have been loaded.");
			});
		}
		else
			UnityEngine.Debug.Log ("Failed to authenticate with Game Center.");
	}
	
	/// <summary>
	/// Get the leaderboard.
	/// </summary>
	void DoLeaderboard () {
		m_Leaderboard = Social.CreateLeaderboard();
		m_Leaderboard.id = GameIds.GCleaderboardId;  // YOUR CUSTOM LEADERBOARD NAME
		m_Leaderboard.LoadScores(result => DidLoadLeaderboard(result));
	}
	
	/// <summary>
	/// RETURNS THE NUMBER OF LEADERBOARD SCORES THAT WERE RECEIVED BY THE APP
	/// </summary>
	/// <param name="result">If set to <c>true</c> result.</param>
	void DidLoadLeaderboard (bool result) {
		UnityEngine.Debug.Log("Received " + m_Leaderboard.scores.Length + " scores");
		foreach (IScore score in m_Leaderboard.scores) {
			UnityEngine.Debug.Log(score);
		}
		//Social.ShowLeaderboardUI();
	}
	
	// THIS FUNCTION GETS CALLED WHEN THE LoadAchievements CALL COMPLETES
	void ProcessLoadedAchievements (IAchievement[] achievements) {
		if (achievements.Length == 0)
			UnityEngine.Debug.Log ("Error: no achievements found");
		else
			UnityEngine.Debug.Log ("Got " + achievements.Length + " achievements");
		
		// You can also call into the functions like this
		Social.ReportProgress ("Achievement01", 100.0, result => {
			if (result)
				UnityEngine.Debug.Log ("Successfully reported achievement progress");
			else
				UnityEngine.Debug.Log ("Failed to report achievement");
		});

		//Social.ShowAchievementsUI();
	}

	#endregion
}
