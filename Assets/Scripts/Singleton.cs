﻿using UnityEngine;
using System.Collections;

public enum GameState {NullState, MainMenu, Tutorial, SelectTheRightSide, WaitingForTheNextTouch, ResultScreen, Idle}

public delegate void OnStateChangeHandler();

public class GameManager {
	public GameState gameState { get; private set;}

	protected GameManager (){}

	private static GameManager _instance = null;
	public event OnStateChangeHandler OnStateChange;

	
	public static GameManager Instance 
	{
		get 
		{
			if (GameManager._instance == null) 
			{
				GameManager._instance = new GameManager(); 
			}  
			return GameManager._instance;
		} 
	}

	public void SetGameState(GameState gameState)
	{
		this.gameState = gameState;

		if(OnStateChange!=null)
		{
			OnStateChange();
		}
	}
}
