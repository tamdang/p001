﻿using UnityEngine;
using System.Collections;

public static class GameIds  {

	// Achievements IDs (as given by Developer Console)
	public class GPAchievements {
		public const string IAm20 = "CgkIl8mZibULEAIQBA";
		public const string IAm40 = "CgkIl8mZibULEAIQBQ";
		public const string IAm60 = "CgkIl8mZibULEAIQBg";
		public const string ALifeTime = "CgkIl8mZibULEAIQCg";
		public const string LeftHandPerson = "CgkIl8mZibULEAIQAg";
		public const string RightHandPerson = "CgkIl8mZibULEAIQAw";
		public const string LeftThankGiving = "CgkIl8mZibULEAIQCA";
		public const string RightThankGiving = "CgkIl8mZibULEAIQCQ";
		public const string OneTwo = "CgkIl8mZibULEAIQBw";
	}
	
	// Leaderboard ID (as given by Developer Console)
	public readonly static string GPLeaderboardId = "CgkIl8mZibULEAIQAQ";

	public class KeysInFileSave
	{
		public readonly static string CountGamePlayed = "CountGamePlayed";
		public readonly static string BestScore = "PlayerBestScore";
		public readonly static string LaterSelectedInRateTheGame = "LaterSelectedInRateTheGame";
		public readonly static string NeedToDisplayRateTheGame = "NeedToDisplayRateTheGame";
		public readonly static string GameSetting = "GameSetting";
	}

	public class ResourceName
	{
		public readonly static string ImageFOLDER = "Images/";
		public readonly static string SoundBGOn = "Images/sound_on";
		public readonly static string SoundBGOff = "Images/sound_off";
		public readonly static string SoundEffectOn = "Images/effect_on";
		public readonly static string SoundEffectOff = "Images/effect_off";
	}
	
	public class GCAchievements
	{
		public const string IAm20 = "com.tamdang.biggernumber.IAm20";
		public const string IAm40 = "com.tamdang.biggernumber.IAm40";
		public const string IAm60 = "com.tamdang.biggernumber.IAm60";
		public const string ALifeTime = "com.tamdang.biggernumber.ALifeTime";
		public const string LeftHandPerson = "com.tamdang.biggernumber.LeftHandPerson";
		public const string RightHandPerson = "com.tamdang.biggernumber.RightHandPerson";
		public const string LeftThankGiving = "com.tamdang.biggernumber.LeftThankGiving";
		public const string RightThankGiving = "com.tamdang.biggernumber.RightThankGiving";
		public const string OneTwo = "com.tamdang.biggernumber.OneTwo";
	}

	public const string GCleaderboardName = "High Score";
	public const string GCleaderboardId = "com.tamdang.biggernumber.highscore";
}
