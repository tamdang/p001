﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GoogleMobileAds.Api;
using System;
#if UNITY_ANDROID
using GooglePlayGames;
#endif
using UnityEngine.SocialPlatforms;

enum Side {Left, Right};

public class GameController : MonoBehaviour {
	
	public List<Text> LeftTexts = new List<Text> ();
	public List<Text> RightTexts = new List<Text> ();
	
	public Text ScoreText;
	public Text BestScoreText;

	public GameObject CountDownClock;
	public GUITexture PlusOne;
	public float TransitionDuration;
	public Canvas TouchLeftRight;
	public float AddingTime;
	public Image GameOverImage;
	AudioSource RightSound;
	AudioSource WrongSound;
	AudioSource BGSound;

	int LeftNumber;
	int RightNumber;
	int Score = 0;

	public float OriginalWaitingTime;
	public float MinWaitingTime;
	public int MixingRate;
	public Button StartButton;
	public GameObject APPanel;
	public GameObject PlaySettingButtons;
	public int NumOfPlayToDisplayInterstitial;
	public GameObject UIManager;
	public GameObject NewRecordHighLight; 

	float WaitingTime;
	bool IsWaitingFirstTime = true;

	Color ActiveNumberColor;
	Color ActiveNumberColorGameOver;
	
	int countLeft = 0;
	int countRight = 0;

	int countOneTwo = 0;
	int countScoreLeft = 0;
	int countScoreRight = 0;
	int lastScoreSide = -1; // 0: Left, 1: Right
	int CurrentScoreSide = -1; // 0: Left, 1: Right


	Side wrongSide;
	
	public GameObject LeftArrow;
	public GameObject RightArrow;
	public float ArrowAnimationTime;
	public Button soundBGButton;
	public Button soundEffectButton;

	int activeIndex = 0;

	bool soundBGEnable = true;
	bool soundEffectEnable = true;
	
	// Use this for initialization
	void Start () {
		WaitingTime = OriginalWaitingTime;
		BestScoreText.text = "";
		ScoreText.text = Score.ToString();

		AudioSource[] audiosources = GetComponents<AudioSource>();
		WrongSound = audiosources [0];
		RightSound = audiosources [1];
		BGSound = audiosources [2];

		GameManager.Instance.SetGameState(GameState.MainMenu);

		APPanel.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Images/BG_MM");

		GameOverImage.enabled = false;

		ActiveNumberColor = LeftTexts [activeIndex].color;
		ActiveNumberColorGameOver = LeftTexts [1].color;

		SocialManager.Authenticate ();

		AdsManager.Instance.RequestInterstitial();

		LoadGameSetting ();

		if (Screen.height <= 960) {

			LeftTexts [0].resizeTextMaxSize = LeftTexts [1].resizeTextMaxSize = 
				RightTexts [0].resizeTextMaxSize = RightTexts [1].resizeTextMaxSize = 
					ScoreText.resizeTextMaxSize = BestScoreText.resizeTextMaxSize = 65;
		}
		else if (Screen.height <= 1136)
		{
			LeftTexts [0].resizeTextMaxSize = LeftTexts [1].resizeTextMaxSize = 
				RightTexts [0].resizeTextMaxSize = RightTexts [1].resizeTextMaxSize = 
					ScoreText.resizeTextMaxSize = BestScoreText.resizeTextMaxSize = 85;
		}
		else if (Screen.height <= 1334)
		{
			LeftTexts [0].resizeTextMaxSize = LeftTexts [1].resizeTextMaxSize = 
				RightTexts [0].resizeTextMaxSize = RightTexts [1].resizeTextMaxSize = 
					ScoreText.resizeTextMaxSize = BestScoreText.resizeTextMaxSize = 130;
		}
		else
		{
			LeftTexts [0].resizeTextMaxSize = LeftTexts [1].resizeTextMaxSize = 
				RightTexts [0].resizeTextMaxSize = RightTexts [1].resizeTextMaxSize = 
					ScoreText.resizeTextMaxSize = BestScoreText.resizeTextMaxSize = 130;
		}
	}

	void OnApplicationPause(bool pauseStatus) {
//		paused = pauseStatus;

		if(pauseStatus)
		{
			if(GameManager.Instance.gameState != GameState.Idle && 
			   GameManager.Instance.gameState != GameState.ResultScreen && 
			   GameManager.Instance.gameState != GameState.Tutorial)
			{
				for(int i=0;i<2;i++)
				{
					GenerateNewNumbers(i);
				}
			}
		}
	}

	void TimeIsUpListener(float StartTime)
	{
		GameManager.Instance.SetGameState (GameState.ResultScreen);
	}

	// Update is called once per frame
	void Update () {

		switch(GameManager.Instance.gameState)
		{
		case GameState.MainMenu:
			TouchLeftRight.enabled = false;
			Timer c = CountDownClock.GetComponent<Timer>();
			CountDownClock.SetActive(false);
			GameOverImage.enabled = false;
			ScoreText.text = "";
			for(int i=0; i<2; i++)
			{
				LeftTexts[i].text = RightTexts[i].text = "";
			}

			break;

		case GameState.Tutorial:
			if(IsWaitingFirstTime)
			{
				if(!AdsManager.Instance.IsAdBannerLoadedSuccessfully)
				{
					AdsManager.Instance.RequestBanner();
				}
				GameOverImage.enabled = false;
				BGSound.Play();
				c = CountDownClock.GetComponent<Timer>();
				c.SetTime(OriginalWaitingTime/1000);
				CountDownClock.SetActive(true);
				IsWaitingFirstTime = false;

				WaitingTime = OriginalWaitingTime;
				BestScoreText.text = "";
				ScoreText.text = Score.ToString();

				for(int i=0; i<2; i++)
				{
					GenerateNewNumbers(i);
				}
			}
			
			if(Input.GetMouseButtonDown(0))
			{
				TouchHandler(new Vector2(Input.mousePosition.x,Input.mousePosition.y));
			}
			break;

		case GameState.WaitingForTheNextTouch:
			if(Input.GetMouseButtonDown(0))
			{
				TouchHandler(new Vector2(Input.mousePosition.x,Input.mousePosition.y));
			}
			break;

		case GameState.SelectTheRightSide:
			LeftTexts[activeIndex].text = LeftTexts[1].text;
			RightTexts[activeIndex].text = RightTexts[1].text;
			GenerateNewNumbers(1);
			GameManager.Instance.SetGameState(GameState.WaitingForTheNextTouch);


			break;

		case GameState.ResultScreen:
			GameOver();
			break;

		case GameState.Idle:
			break;
		}
	}
	
	public void OnPlay()
	{
		if(GameManager.Instance.gameState!= GameState.MainMenu)
		{
			LeftTexts[activeIndex].color = RightTexts[activeIndex].color = ActiveNumberColor;
		}
		else
		{
			APPanel.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Images/BG");

		}

		GameManager.Instance.SetGameState(GameState.Tutorial);
		TouchLeftRight.enabled = true;
		
		StartCoroutine("MoveLeftAnimation",LeftArrow);
		StartCoroutine("MoveRightAnimation",RightArrow);

		LeftTexts [1].enabled = RightTexts [1].enabled = true;

		CountDownClock.SetActive (true);
		CountDownClock.GetComponent<Timer> ().ResetTimer ();
	}
	
	void CheckAchievements()
	{
		if(RightNumber > LeftNumber)
		{
			countLeft =0;
			countRight ++;
			countScoreRight++;

			if(countScoreRight == 40)
			{
				SocialManager.UnlockAchievement(AchievementId.RightThankGiving);
			}

			if(countRight == 8)
			{
				SocialManager.UnlockAchievement(AchievementId.RightHandPerson);
			}

			CurrentScoreSide = 1;
		}
		else
		{
			countRight = 0;
			countLeft ++;
			countScoreLeft++;

			if(countScoreLeft == 40)
			{
				SocialManager.UnlockAchievement(AchievementId.LeftThankGiving);
			}

			if(countLeft == 8)
			{
				SocialManager.UnlockAchievement(AchievementId.LeftHandPerson);
			}

			CurrentScoreSide = 0;
		}

		if(lastScoreSide < 0)
		{
			lastScoreSide = CurrentScoreSide;
		}
		else if(lastScoreSide!=CurrentScoreSide)
		{
			countOneTwo ++;
			lastScoreSide = CurrentScoreSide;
			if (countOneTwo == 10) 
			{
				SocialManager.UnlockAchievement(AchievementId.OneTwo);
			}
		}
		else
		{
			lastScoreSide = CurrentScoreSide;
			countOneTwo = 0;
		}

		if(Score==20)
		{
			SocialManager.UnlockAchievement(AchievementId.IAm20);
		}
		else if(Score==40)
		{
			SocialManager.UnlockAchievement(AchievementId.IAm40);
		}
		else if(Score==60)
		{
			SocialManager.UnlockAchievement(AchievementId.IAm60);
		}
		else if(Score==100)
		{
			SocialManager.UnlockAchievement(AchievementId.ALifeTime);
		}
	}

	void TouchHandler(Vector2 position)
	{
		RightNumber = int.Parse (RightTexts [activeIndex].text);
		LeftNumber = int.Parse (LeftTexts [activeIndex].text);

		if((position.x > Screen.width/2 && RightNumber>LeftNumber) ||
		   (position.x <= Screen.width/2 && RightNumber<LeftNumber))
		{
			RightSound.Play();
			Score++;
			ScoreText.text = Score.ToString();

			CheckAchievements();

			Timer c = CountDownClock.GetComponent<Timer>();

			if(GameManager.Instance.gameState == GameState.Tutorial)
			{
				c.SetTime (WaitingTime/1000);
				c.StartCountDown();
				TouchLeftRight.enabled = false;
			}
			
			c.UpdateTimeRemaining (AddingTime);
			c.StartBonuseffect();

			GameManager.Instance.SetGameState(GameState.SelectTheRightSide);
		}
		else
		{
			WrongSound.Play();
			if(GameManager.Instance.gameState!=GameState.Tutorial)
			{
				GameManager.Instance.SetGameState(GameState.ResultScreen);
			}
			else
			{
//				GameManager.Instance.SetGameState(GameState.TutorialWrongSide);
				if(position.x > Screen.width/2)
				{
					wrongSide = Side.Right;
				}
				else
				{
					wrongSide = Side.Left;
				}
				WrongSideTouch(activeIndex);
			}
		}
	}

	void GameOver()
	{
		GameManager.Instance.SetGameState(GameState.Idle);
		Timer c = CountDownClock.GetComponent<Timer>();
		c.StopCountDown();

		CountDownClock.SetActive (false);

		GameOverImage.enabled = true;
		LeftTexts[activeIndex].color = RightTexts[activeIndex].color = ActiveNumberColorGameOver;
		BGSound.Stop();

		AdsManager.Instance.CountGamePlayed += 1;

		SaveBestScore ();
		Score = 0;
		IsWaitingFirstTime = true;

		UIEventManager eventManager = UIManager.GetComponent<UIEventManager> ();
		eventManager.OnFlyIn (PlaySettingButtons);

		if(!AdsManager.Instance.IsInterstitialRequestedSuccessfully)
		{
			AdsManager.Instance.RequestInterstitial();
		}

		if (AdsManager.Instance.CountGamePlayed >= NumOfPlayToDisplayInterstitial)
		{
			AdsManager.Instance.ShowInterstitial();
			AdsManager.Instance.CountGamePlayed = 0;
		}

		countOneTwo = 0;
		countScoreLeft = 0;
		countScoreRight = 0;
		lastScoreSide = -1; // 0: Left, 1: Right
		CurrentScoreSide = -1; // 0: Left, 1: Right
		countLeft = 0;
		countRight = 0;

		LeftTexts [1].enabled = RightTexts [1].enabled = false;
	}

	void LoadGameSetting()
	{
		if(PlayerPrefs.HasKey(GameIds.KeysInFileSave.GameSetting))
		{
			int gameSetting = PlayerPrefs.GetInt(GameIds.KeysInFileSave.GameSetting);
			soundBGEnable = gameSetting / 2 > 0;
			soundEffectEnable = gameSetting % 2 > 0;
			BGSound.mute = !soundBGEnable;
			WrongSound.mute = RightSound.mute = !soundEffectEnable;

			string soundBGSpriteName = soundBGEnable? GameIds.ResourceName.SoundBGOn : GameIds.ResourceName.SoundBGOff;
			string soundEffectSpriteName = soundEffectEnable?GameIds.ResourceName.SoundEffectOn : GameIds.ResourceName.SoundEffectOff;

			soundBGButton.image.sprite = Resources.Load<Sprite>(soundBGSpriteName);
			soundEffectButton.image.sprite = Resources.Load<Sprite>(soundEffectSpriteName);
		}

	}
	
	public void SaveGameSetting()
	{
		int gameSetting = 0;
		gameSetting += soundBGEnable?2:0;
		gameSetting += soundEffectEnable?1:0;

		PlayerPrefs.SetInt(GameIds.KeysInFileSave.GameSetting, gameSetting);
	}


	void SaveBestScore()
	{
		if(PlayerPrefs.HasKey(GameIds.KeysInFileSave.BestScore))
		{
			int PlayerBestScore = PlayerPrefs.GetInt(GameIds.KeysInFileSave.BestScore);
			if (Score>PlayerBestScore)
			{
				PlayerPrefs.SetInt(GameIds.KeysInFileSave.BestScore, Score);
				SocialManager.ReportScore(Score);
				BestScoreText.text = "New best"; 

				NewRecordHighLight.SetActive(true);
			}
			else
			{
				BestScoreText.text = "Best " + PlayerBestScore.ToString(); 
				NewRecordHighLight.SetActive(false);
			}
		}
		else
		{
			PlayerPrefs.SetInt(GameIds.KeysInFileSave.BestScore, Score);
			if(Score > 0)
			{
				SocialManager.ReportScore(Score);
				NewRecordHighLight.SetActive(false);
			}
		}
	}
	
	void GenerateNewNumbers(int textIndex)
	{
		NumberGenerator.GenerateNumbers (Score, MixingRate, out LeftNumber, out RightNumber);

		LeftTexts[textIndex].text = LeftNumber.ToString ();
		RightTexts[textIndex].text = RightNumber.ToString ();
	}

	public void MuteSound(bool value)
	{
		BGSound.mute = value;
		soundBGEnable = !value;
	}

	public void MuteSFX(bool value)
	{
		RightSound.mute = WrongSound.mute = value;
		soundEffectEnable = !value;
	}

	void WrongSideTouch(int activeIndex)
	{
		Text activeSmaller = null;
		Text activeBigger = null;
		if(wrongSide == Side.Left)
		{
			activeSmaller = LeftTexts[activeIndex];
			activeBigger = RightTexts[activeIndex];
		}
		else
		{
			activeSmaller = RightTexts[activeIndex];
			activeBigger = LeftTexts[activeIndex];
		}
		StartCoroutine("FadeInOutRed",activeSmaller);
		StartCoroutine("FadeInOutGreen",activeBigger);
	}

	IEnumerator FadeInOutRed(Text text) {
		float t = 0;
		float alpha = 0;
		while(t<TransitionDuration)
		{
			t+=Time.deltaTime*1000;
			alpha = MotionScripts.easeInOutQuad(t,1,-1,TransitionDuration);
			text.color = new Color(1,0, 0, alpha);
			yield return null;
		}
		
		t = 0;
		
		while(t<TransitionDuration)
		{
			t+=Time.deltaTime*1000;
			alpha = MotionScripts.easeInOutQuad(t,0,1,TransitionDuration);
			text.color = new Color(1,0, 0, alpha);
			yield return null;
		}
		
		alpha = 1;
		text.color = new Color(1,1, 1, alpha);
	}

	IEnumerator FadeInOutGreen(Text text) {
		float t = 0;
		float alpha = 0;
		while(t<TransitionDuration)
		{
			t+=Time.deltaTime*1000;
			alpha = MotionScripts.easeInOutQuad(t,1,-1,TransitionDuration);
			text.color = new Color(0,1, 0, alpha);
			yield return null;
		}
		
		t = 0;
		
		while(t<TransitionDuration)
		{
			t+=Time.deltaTime*1000;
			alpha = MotionScripts.easeInOutQuad(t,0,1,TransitionDuration);
			text.color = new Color(0,1, 0, alpha);
			yield return null;
		}
		
		alpha = 1;
		text.color = new Color(1,1, 1, alpha);
	}

	IEnumerator MoveLeftAnimation(GameObject panel) {
		Image panelImage = panel.GetComponent<Image> ();
		int OFFSET = 30;
		float t = 0;
		Vector2 anchoredPosition0 = panelImage.rectTransform.anchoredPosition;
		Vector2 anchoredPosition1 = new Vector2 (panelImage.rectTransform.anchoredPosition.x-OFFSET, panelImage.rectTransform.anchoredPosition.y);
		while(t<ArrowAnimationTime && GameManager.Instance.gameState == GameState.Tutorial)
		{
			t+=Time.deltaTime*1000;
			panelImage.rectTransform.anchoredPosition = MotionScripts.easeInOutQuad(t,anchoredPosition0,anchoredPosition1-anchoredPosition0,ArrowAnimationTime);
			if(t>=ArrowAnimationTime) t = 0;
			yield return null;
		}
		t = 0;
		panelImage.rectTransform.anchoredPosition = anchoredPosition0;
	}

	IEnumerator MoveRightAnimation(GameObject panel) {
		Image panelImage = panel.GetComponent<Image> ();
		int OFFSET = 30;
		float t = 0;
		Vector2 anchoredPosition0 = panelImage.rectTransform.anchoredPosition;
		Vector2 anchoredPosition1 = new Vector2 (panelImage.rectTransform.anchoredPosition.x+OFFSET, panelImage.rectTransform.anchoredPosition.y);
		while(t<ArrowAnimationTime && GameManager.Instance.gameState == GameState.Tutorial)
		{
			t+=Time.deltaTime*1000;
			panelImage.rectTransform.anchoredPosition = MotionScripts.easeInOutQuad(t,anchoredPosition0,anchoredPosition1-anchoredPosition0,ArrowAnimationTime);
			if(t>=ArrowAnimationTime) t = 0;
			yield return null;
		}
		t = 0;
		panelImage.rectTransform.anchoredPosition = anchoredPosition0;
	}
}
